import json
import time

import requests

blood_banks = ['61334be91155d8047d5f1e6d', '5d5e9051bce1e56f5163ac8e', '614db1231155d8047d5f1ec7']
refresh_time_secs = 10
past_camps_count = [
    {
        'Name': 'Camp1',
        'Count': 229
    },
    {
        'Name': 'Camp2',
        'Count': 307
    }]


def get_latest_count():
    headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
    request_payload = {"page": 1, "limit": "50", "camp": "6193a3e7a397423a4bd07ce1",
                       "venue": "60780c5388fba113fa0257c3", "hospital": "61334be91155d8047d5f1e6d", "donorId": "",
                       "name": "", "firstName": "", "middleName": "", "lastName": "", "mobileNo": "",
                       "accessLevel": "entry"}
    total_count_in_all_camps = sum(val.get('Count') for val in past_camps_count)
    total_count_in_this_camp = 0
    for blood_bank in blood_banks:
        request_payload['hospital'] = blood_bank
        try:
            resp = requests.post('https://api.thetmm.org/CampDonation/totalEntryAllForEntry', headers=headers,
                                 data=json.dumps(request_payload))
            if resp and resp.status_code == 200:
                json_resp = resp.json()
                bank_count = json_resp.get('count')
                if bank_count == 0:
                    print("")
                else:
                    total_count_in_this_camp = total_count_in_this_camp + bank_count
        except Exception as e:
            print(f"Count fetch failure for blood bank {blood_bank}. Error {str(e)}")
    total_count_in_all_camps = total_count_in_all_camps + total_count_in_this_camp
    print(f"Total count in all camps: {total_count_in_all_camps}")
    print(f"Total count in this camp: {total_count_in_this_camp}")


if __name__ == '__main__':
    while True:
        get_latest_count()
        time.sleep(refresh_time_secs)
