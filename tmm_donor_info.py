import json

import pandas as pd
import requests


def get_donor_list(file_name):
    temp = pd.read_excel(file_name, engine='openpyxl')
    return temp['Donor Name'].tolist()


def get_tmm_donor_data(donors_list):
    donors = []
    headers = {'Content-Type': 'application/json', 'Accept':'application/json'}
    donor_req_obj = {"page": 1, "limit": "50", "camp": "", "campNumber": "", "donorId": "",
                     "name": "ABDUL MUNAWWAR ANSARI", "firstName": "", "middleName": "", "lastName": "",
                     "pincode": "", "mobileNo": "", "accessLevel": "entry"}
    donor_detail_req_obj = {
        "donor": "6097c33b4e180d2723faf852",
        "camp": "6193a3e7a397423a4bd07ce1"
    }
    for idx, name in enumerate(donors_list):
        try:
            donor_req_obj['name'] = name
            resp = requests.post('https://api.thetmm.org/Donor/getAllEntry', headers=headers,
                                 data=json.dumps(donor_req_obj))
            donor_id = resp.json()['result'][0]['_id']
            donor_detail_req_obj['donor'] = donor_id
            resp = requests.post('https://api.thetmm.org/Donor/getOneDonorAndCampDonation', headers=headers,
                                 data=json.dumps(donor_detail_req_obj))
            tmm_donor_obj = resp.json().get('donor')
            tmm_donation_count = resp.json().get('donationCount')
            mobile_no = tmm_donor_obj.get('mobileNo')
            full_name = tmm_donor_obj.get('name')
            gender = tmm_donor_obj.get('gender')
            donor_obj = dict()
            donor_obj['name'] = name
            donor_obj['mobile'] = int(mobile_no)
            donor_obj['Gender'] = gender
            print(idx)
            if tmm_donation_count == 'No Data Found' or tmm_donation_count.get('donationCount') == 0:
                donor_obj['Donation Count'] = 0
            else:
                donor_obj['Donation Count'] = int(tmm_donation_count.get('donationCount'))
            donors.append(donor_obj)
        except Exception as e:
            print(f"Error occurred for donor {name}: {str(e)}")
    return donors


if __name__ == '__main__':
    donors_list = get_donor_list('tmm_donor12122021.xlsx')
    donors = get_tmm_donor_data(donors_list)
    print(donors)
